document.addEventListener("DOMContentLoaded", setup);

let container;
let todo = [];

//Add the event listeners
function setup() {
  container = document.querySelector("#container");
  loadTodo();

  let btnPost = document.querySelector("#post");
  btnPost.addEventListener("click", submit);

  let inputs = document.querySelectorAll('input[type=checkbox]');
  for (i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("click", checkedAction);
  }
}

//Activates when button submit is clicked
function submit() {
  //Creates the object to the todo array
  let task = {
    title: document.getElementById("taskTitle").value,
    description: document.getElementById("description").value,
    importance: document.getElementById("importance").value,
    category: detectCategory()
  };

  todo.push(task);

  saveInStorage();
  //Creates the actual html for the object
  appendTodo(task);
}

//Save in local storage
function saveInStorage() {
  localStorage.setItem("toDoJSON", JSON.stringify(todo));
}

//Creates the actual html for the object
function appendTodo(task) {
  //Creates the elements
  let taskHolder = document.createElement("div");
  let leftBar = document.createElement("div");
  let contentHolder = document.createElement("div");
  let taskFirstLine = document.createElement("div");
  let titleHolder = document.createElement("h3");
  let starsHolder = document.createElement("p");
  let taskCheckbox = document.createElement('input');

  let descriptionHolder = document.createElement("div");
  let taskDescription = document.createElement("p");

  //Creates the id and classes
  taskHolder.classList.add("createdTask");
  leftBar.classList.add("colorBar");
  contentHolder.classList.add("content");
  taskFirstLine.classList.add("firstLine");
  taskCheckbox.classList.add("remove");
  taskHolder.id = todo.indexOf(task);

  //Add other important thing
  taskCheckbox.type = "checkbox";
  taskCheckbox.addEventListener("click", checkedAction);
  leftBar.style.backgroundColor = determineColor(task.category);

  //Create the content
  let taskTitle = document.createTextNode(task.title);
  let taskStars = document.createTextNode(determineStars(task.importance));
  let descriptionContent = document.createTextNode(task.description);

  //Appends everything
  container.appendChild(taskHolder);
  taskHolder.appendChild(leftBar);
  taskHolder.appendChild(contentHolder);
  contentHolder.appendChild(taskFirstLine);
  contentHolder.appendChild(descriptionHolder);
  descriptionHolder.appendChild(taskDescription);
  taskDescription.appendChild(descriptionContent);
  taskFirstLine.appendChild(titleHolder);
  taskFirstLine.appendChild(starsHolder);
  taskFirstLine.appendChild(taskCheckbox);
  titleHolder.appendChild(taskTitle);
  starsHolder.appendChild(taskStars);
}

//Give the Category name
function detectCategory() {
  let type = document.getElementsByName("category");
  let category;
  if (type[0].checked) {
    category = type[0].value;
  }
  else if (type[1].checked) {
    category = type[1].value;
  }
  else if (type[2].checked) {
    category = type[2].value;
  }
  return category;
}

//Give the color associate to the category
function determineColor(type) {
  let color;
  if (type == "School") {
    color = "mediumpurple";
  }
  else if (type == "Work") {
    color = "deepskyblue";
  }
  else if (type == "Personal") {
    color = "lightpink";
  }
  return color;
}

//Fills the stars
function determineStars(importance) {
  let stars;
  if (importance == "3") {
    stars = '\u2605 \u2605 \u2605';
  }
  else if (importance == "1") {
    stars = '\u2605 \u2606 \u2606';

  }
  else if (importance == "2") {
    stars = '\u2605 \u2605 \u2606';
  }
  else {
    stars = '\u2606 \u2606 \u2606';
  }
  return stars;
}

//Remove div from storage and DOM when checked
function checkedAction() {
  let checkedValue = document.querySelector("input[type=checkbox]:checked");
  let taskHolder = checkedValue.parentElement.parentElement.parentElement;
  let index = parseInt(taskHolder.id);
  taskHolder.remove();
  todo.splice(index, 1);
  //To update the divs ID;
  location.reload();
  saveInStorage();
}

//Updates the page to correspond to what is in the local storage
function loadTodo() {
  if (localStorage.getItem("toDoJSON")) {
    todo = JSON.parse(localStorage.getItem("toDoJSON"));
  }
  for (i = 0; i < todo.length; i++) {
    appendTodo(todo[i]);
  }
}
